cabal-version:       3.0
name:                fir-examples
version:             0.0.1.0
synopsis:            Vulkan example scenes using FIR
category:            Graphics
author:              Sam Derbyshire
maintainer:          sam.derbyshire@gmail.com
license:             BSD-3-Clause
homepage:            https://gitlab.com/sheaf/fir/fir-examples
build-type:          Simple
description:

  This package contains some example Vulkan scenes,
  using shaders compiled by __FIR__.

  Depends on the 'vulkan-api' package for Vulkan bindings.

  See the root directory for more information about __FIR__,
  an EDSL for writing shaders that compile to __@SPIR-V@__.
data-files:
  assets/fir_logo.png
  assets/haskell_logo.png

----------------------------------------------------------------
-- Common stanzas.

common base-common

    build-depends:
        base
          >= 4.13     && < 4.16
      , directory
          ^>= 1.3.3.0
      , filepath
          ^>= 1.4.2.1
      , finite-typelits
          ^>= 0.1.4.2
      , template-haskell
          >= 2.15     && < 2.18
      , text-short
          >= 0.1.2    && < 0.2
      , vector
          ^>= 0.12.0.3
      , vector-sized
          ^>= 1.4.0.0

    default-language:
      Haskell2010

    default-extensions:
      NoStarIsType

    ghc-options:
      -Wall
      -Wcompat
      -fwarn-missing-local-signatures
      -fwarn-incomplete-uni-patterns
      -fno-warn-unticked-promoted-constructors


common vulkan-common

  import: base-common

  build-depends:
      bytestring
        >= 0.10.9.0 && < 0.11
    , JuicyPixels
        ^>= 3.3.2
    , lens
        >= 4.18     && < 4.20
    , logging-effect
        ^>= 1.3.6
    , resourcet
        ^>= 1.2.2
    , sdl2
        >= 2.5.0 && <= 2.6
    , transformers
        ^>= 0.5.6.2
    , vulkan-api
        >= 1.1.4 && <= 1.4

common apps-common

    import: vulkan-common

    hs-source-dirs:
        examples/apps

    build-depends:
        fir
      , fir-examples
      , fir-examples-paths

    ghc-options:
        -threaded -rtsopts

    -- bypass TH segfault bug on Windows, see (GHC issue #13112)[https://gitlab.haskell.org/ghc/ghc/issues/13112]
    --if os(windows)
    --  ghc-options:
    --    -fexternal-interpreter

common shaders-common

    import: base-common

    hs-source-dirs:
        examples/shaders

    ghc-options:
      -fno-warn-partial-type-signatures
      -fno-warn-unused-do-bind

    build-depends:
        fir
      , fir-examples-paths


----------------------------------------------------------------
-- Shared library components used by all the examples.


library fir-examples-paths

    import: base-common

    hs-source-dirs:
        paths

    exposed-modules:
        FIR.Examples.Paths

    autogen-modules:
        Paths_fir_examples

    other-modules:
        Paths_fir_examples
      , FIR.Examples.Paths.CreateDirs

library

    import: vulkan-common

    hs-source-dirs:
        src

    exposed-modules:
        Data.Traversable.Indexed

      -- Shared examples code
      , FIR.Examples.Common
      , FIR.Examples.Reload
      , FIR.Examples.RenderState

      -- Vulkan backend code
      , Vulkan.Attachment
      , Vulkan.Backend
      , Vulkan.Buffer
      , Vulkan.Context
      , Vulkan.Features
      , Vulkan.Formats
      , Vulkan.Memory
      , Vulkan.Monad
      , Vulkan.Pipeline
      , Vulkan.Resource
      , Vulkan.Screenshot
      , Vulkan.SDL

    build-depends:
        fir
      , fir-examples-paths
      , containers
          ^>= 0.6.2.1
      , fsnotify
          ^>= 0.3.0.1
      , generic-lens
          >= 1.2.0.1 && < 2.0
      , mtl
          ^>= 2.2.2
      , stm
          ^>= 2.5.0.0
      , unliftio-core
          >= 0.1.2.0 && < 0.3


----------------------------------------------------------------
-- Examples.

-----------------------------------
-- Bezier

library bezier-shaders

  import: shaders-common

  exposed-modules:
    FIR.Examples.Bezier.Shaders

executable Bezier

  import: apps-common

  main-is:
    Main.hs

  other-modules:
    FIR.Examples.Bezier.Application

  hs-source-dirs:
    examples/exes/Bezier

  build-depends:
    bezier-shaders


-----------------------------------
-- FullPipeline

library fullpipeline-shaders

  import: shaders-common

  exposed-modules:
    FIR.Examples.FullPipeline.Shaders

executable FullPipeline

  import: apps-common

  main-is:
    Main.hs

  other-modules:
    FIR.Examples.FullPipeline.Application

  hs-source-dirs:
    examples/exes/FullPipeline

  build-depends:
    fullpipeline-shaders


-----------------------------------
-- Hopf

library hopf-shaders

  import: shaders-common

  exposed-modules:
    FIR.Examples.Hopf.Shaders

executable Hopf

  import: apps-common

  main-is:
    Main.hs

  other-modules:
      FIR.Examples.Hopf.Application
    , FIR.Examples.Hopf.Villarceau

  hs-source-dirs:
    examples/exes/Hopf

  build-depends:
    hopf-shaders


-----------------------------------
-- JuliaSet

library juliaset-shaders

  import: shaders-common

  exposed-modules:
    FIR.Examples.JuliaSet.Shaders

executable JuliaSet

  import: apps-common

  main-is:
    Main.hs

  other-modules:
    FIR.Examples.JuliaSet.Application

  hs-source-dirs:
    examples/exes/JuliaSet

  build-depends:
    juliaset-shaders


-----------------------------------
-- Kerr

library kerr-shaders

  import: shaders-common

  exposed-modules:
    -- Shader module needs to be first.
      FIR.Examples.Kerr.Shaders
    , FIR.Examples.Kerr.Colour
    , FIR.Examples.Kerr.Coordinates
    , FIR.Examples.Kerr.Doppler
    , FIR.Examples.Kerr.Event
    , FIR.Examples.Kerr.Info
    , FIR.Examples.Kerr.Motion
    , FIR.Examples.Kerr.Noise
    , FIR.Examples.Kerr.RungeKutta

executable Kerr

  import: apps-common

  main-is:
    Main.hs

  other-modules:
    FIR.Examples.Kerr.Application

  hs-source-dirs:
    examples/exes/Kerr

  build-depends:
    kerr-shaders


-----------------------------------
-- Logo

library logo-shaders

  import: shaders-common

  exposed-modules:
    FIR.Examples.Logo.Shaders

executable Logo

  import: apps-common

  main-is:
    Main.hs

  other-modules:
    FIR.Examples.Logo.Application

  hs-source-dirs:
    examples/exes/Logo

  build-depends:
    logo-shaders


-----------------------------------
-- Offscreen

library offscreen-shaders

  import: shaders-common

  exposed-modules:
    FIR.Examples.Offscreen.Shaders

executable Offscreen

  import: apps-common

  main-is:
    Main.hs

  other-modules:
    FIR.Examples.Offscreen.Application

  hs-source-dirs:
    examples/exes/Offscreen

  build-depends:
    offscreen-shaders


-----------------------------------
-- Texture

library texture-shaders

  import: shaders-common

  exposed-modules:
    FIR.Examples.Texture.Shaders

executable Texture

  import: apps-common

  main-is:
    Main.hs

  other-modules:
    FIR.Examples.Texture.Application

  hs-source-dirs:
    examples/exes/Texture

  build-depends:
    texture-shaders

-----------------------------------
-- Transpose
library transpose-shaders

  import: shaders-common

  exposed-modules:
    FIR.Examples.Transpose.Shaders