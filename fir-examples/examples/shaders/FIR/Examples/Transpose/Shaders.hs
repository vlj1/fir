{-# LANGUAGE BlockArguments        #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveFunctor         #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RebindableSyntax      #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE AllowAmbiguousTypes   #-}

module FIR.Examples.Transpose.Shaders where

-- base
import qualified Prelude
import Prelude
  ( Functor, map )
import Control.Monad
  ( void )
import Data.Maybe
  ( fromJust )
import GHC.TypeLits
  ( KnownNat )

-- filepath
import System.FilePath
  ( (</>) )

-- vector-sized
import qualified Data.Vector.Sized as Vector
  ( fromList )

-- text-short
import Data.Text.Short
  ( ShortText )

-- fir
import FIR
  hiding ( Triangle )
import Math.Linear

-- fir-examples
import FIR.Examples.Paths
  ( shaderDir )

------------------------------------------------
-- compute shader

type ComputeDefs =
  '[ "ubo"  ':-> Uniform '[ DescriptorSet 1, Binding 0 ]
                    ( Struct
                       '[ "position" ':-> V 3 Float
                        , "right"    ':-> V 3 Float
                        , "up"       ':-> V 3 Float
                        , "forward"  ':-> V 3 Float
                        ]
                    )
    , "sharedfloat" ':-> Workgroup '[] (Array 512 Float)
    , "input"  ':-> Image2D '[ DescriptorSet 0, Binding 1 ]
                    ( RGBA8 UNorm )                    
    , "output"  ':-> Image2D '[ DescriptorSet 0, Binding 1 ]
                    ( RGBA8 UNorm )
    -- global size: 120 * 135 * 1
    , "main" ':-> EntryPoint '[ LocalSize 16 32 1 ]
                    Compute
   ]

index2dTo1d :: forall a. Semiring a => a -> a -> a
index2dTo1d x y = x + 32 * y

loadIntoMemory
  :: forall (s :: ProgramState). ( _ )
  => Code Word32 -> Code Word32 ->
     Code Word32 -> Code Word32 ->
     Program s s ()
loadIntoMemory i_x i_y i_gx i_gy = locally do
    let idx = index2dTo1d i_x i_y
    let idxp16 = index2dTo1d (i_x + 16) i_y

    indexContent <- imageRead @"input" ( Vec2 i_gx i_gy )
    assign @(Name "sharedfloat" :.: AnIndex Word32) idx (view @(Swizzle "x") indexContent)
    indexContent2 <- imageRead @"input" ( Vec2 (i_gx + 16) i_gy )
    assign @(Name "sharedfloat" :.: AnIndex Word32) idxp16 (view @(Swizzle "x") indexContent2)

    pure ()

swapSharedMemory
  :: forall (s :: ProgramState). ( _ )
  => Code Word32 -> Code Word32 ->
     Code Word32 -> Code Word32 ->
     Program s s ()
swapSharedMemory i_x i_y i_gx i_gy = locally do
    let idx = index2dTo1d i_x i_y
    let idxp16 = index2dTo1d (i_x + 16) i_y

    shared_xy <- use @(Name "sharedfloat" :.: AnIndex Word32) idx
    _ <- def @"tmp" @R shared_xy
    assign @(Name "sharedfloat" :.: AnIndex Word32) idx =<< use @(Name "sharedfloat" :.: AnIndex Word32) idx
    assign @(Name "sharedfloat" :.: AnIndex Word32) (index2dTo1d i_y i_x) =<< get @"tmp"

    pure ()

writeIntoMemory
  :: forall (s :: ProgramState). ( _ )
  => Code Word32 -> Code Word32 ->
     Code Word32 -> Code Word32 ->
     Program s s (Code ())
writeIntoMemory i_x i_y i_gx i_gy = locally do
    let idx = index2dTo1d i_x i_y
    let idxp16 = index2dTo1d (i_x + 16) i_y

    tmp <- use @(Name "sharedfloat" :.: AnIndex Word32) idx
    imageWrite @"output" ( Vec2 i_x i_y ) (Vec4 tmp tmp tmp tmp)
    
    tmp <- use @(Name "sharedfloat" :.: AnIndex Word32) idxp16
    imageWrite @"output"
       ( Vec2 (i_x + 16) i_y )
        (Vec4 tmp tmp tmp tmp)



computeShader :: Module ComputeDefs
computeShader = Module $ entryPoint @"main" @Compute do

    ~(Vec3 i_x i_y _) <- get @"gl_LocalInvocationID"
    ~(Vec3 i_gx i_gy _) <- get @"gl_GlobalInvocationID"

    loadIntoMemory i_x i_y i_gx i_gy

    swapSharedMemory i_x i_y i_gx i_gy

    writeIntoMemory i_x i_y i_gx i_gy




------------------------------------------------
-- compiling

compPath :: FilePath
compPath = shaderDir </> "transpose.spv"

compileComputeShader :: IO ( Either ShortText ModuleRequirements )
compileComputeShader = compileTo compPath [] computeShader

compileAllShaders :: IO ()
compileAllShaders = void compileComputeShader
